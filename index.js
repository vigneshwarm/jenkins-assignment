const express = require('express')
const app = express()

app.get('/', (req, res)=>{
  res.send('hi from nodejs')
})

const PORT = 8000
app.listen(PORT, ()=>console.log(`app is live at http://localhost:${PORT}`))
