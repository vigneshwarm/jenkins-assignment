FROM node:10-alpine

WORKDIR /home/node/app

COPY . .

RUN chown -R node /home/node/app

USER node

RUN npm install

EXPOSE 8000

CMD [ "node", "index.js" ]
