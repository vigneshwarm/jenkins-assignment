# README

## ASSIGNMENT

* Create two jenkins job to build the sample NodeJS application and deploy the application in a docker container.
* Store the built docker images in any registry (Ex: Docker/ECR/Nexus,etc.,).
* If the build job gets succeeded then it should trigger the deploy job automatically and we should be able to access the NodeJS application.
